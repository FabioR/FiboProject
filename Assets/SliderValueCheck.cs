﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class SliderValueCheck : MonoBehaviour
{
    public Slider thisSlider;
    public float oldSliderValue;
    float oldSliderMaxValue;
    
    float oldSliderRatio;
    //public UnityEvent callValueChanged;

    float changeVelocity;
    public bool tween;

    public float finalValue;
    // Start is called before the first frame update
    void Start()
    {
        thisSlider = GetComponent<Slider>();
        oldSliderValue = thisSlider.value;
        oldSliderRatio = thisSlider.value/thisSlider.maxValue;
        oldSliderMaxValue = thisSlider.maxValue;
        finalValue = thisSlider.value;
        tween = false;
    }

    // Update is called once per frame
    void Update()
    { 

        if(thisSlider.value != oldSliderValue && tween ==false){
            finalValue = thisSlider.value;
            thisSlider.value = oldSliderValue;
            tween = true;
        }

        if(tween == true){
            thisSlider.value = Mathf.Lerp(thisSlider.value, finalValue, changeVelocity);
            changeVelocity += 0.5f * Time.deltaTime;
            if(Mathf.Round(thisSlider.value*10) == finalValue*10){
                thisSlider.value = Mathf.Round(thisSlider.value);
                oldSliderValue = thisSlider.value;
                tween = false;
                changeVelocity = 0f;

            }

        }
        /*
        if(thisSlider.value != oldSliderValue){
            if(tween == false){
                finalValue = Mathf.Round(thisSlider.value);
                thisSlider.value = Mathf.Round(oldSliderValue*oldSliderRatio*100f)/100f; 
                tween = true;
            }
        }

        if(tween == true){  
            if((Mathf.Round(thisSlider.value*100f)/100f) != finalValue){

                if(thisSlider.value < finalValue){
                    thisSlider.value = thisSlider.value + changeVelocity;
                    Mathf.Round(thisSlider.value);
                    Debug.Log("Somou no slider: "+ thisSlider.value);
                    if((Mathf.Round(oldSliderRatio*oldSliderValue*100f)/100f) == thisSlider.value){    
                        thisSlider.value = Mathf.Round(thisSlider.value);
                        tween = false;
                    }
                }else{
                    thisSlider.value = thisSlider.value - changeVelocity; 
                    
                    Mathf.Round(thisSlider.value);                 
                    Debug.Log("Subtraiu no slider: "+ thisSlider.value);
                    if((Mathf.Round(oldSliderRatio*oldSliderValue*100f)/100f) == thisSlider.value){    
                        thisSlider.value = Mathf.Round(thisSlider.value);
                        tween = false;
                    }
                }



            }else{
                Debug.Log("terminamos");
                tween = false;
                thisSlider.value = Mathf.Round(thisSlider.value);
                oldSliderValue = thisSlider.value;
                oldSliderRatio = thisSlider.value/thisSlider.maxValue;
            }
        }
    */
    }
}
