﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using TMPro;

public class FBDatabase : MonoBehaviour
{
    DatabaseReference reference;
    public TMPro.TMP_InputField testScoreInput;

    // Start is called before the first frame update
    void Start()
    {
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://fibo-9c479.firebaseio.com/");

        // Get the root reference location of the database.
        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveDataTest(){
        reference.Child("users").Child(Authentication._userId).Child(Authentication._userEmail).SetValueAsync(testScoreInput.text);
    }
}
