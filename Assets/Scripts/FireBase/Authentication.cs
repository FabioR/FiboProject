﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Authentication : MonoBehaviour
{
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;
    string displayName;
    public TMPro.TMP_InputField inputFieldEmail;
    public TMPro.TMP_InputField inputFieldPassword;

    public Button confirmNewUserButton;

    public static string _userId;
    public static string _userEmail;


    // Start is called before the first frame update
    void Start()
    {
        InitializeFirebase();
        _userId = user.UserId;
        _userEmail = user.Email;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitializeFirebase() {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs) {
    if (auth.CurrentUser != user) {
        bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;

        if (!signedIn && user != null) {
            Debug.Log("Signed out " + user.UserId);
        }

        user = auth.CurrentUser;
            if (signedIn) {
                Debug.Log("Signed in " + user.UserId);
                displayName = user.DisplayName ?? "";
                //emailAddress = user.Email ?? "";
                //photoUrl = user.PhotoUrl ?? "";
            }
        }
        Invoke("GetSessionProfile",2);
    }

    public void CreateUserByEmail(){
        string email = inputFieldEmail.text;
        string password = inputFieldPassword.text;

        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
        if (task.IsCanceled) {
            Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
            return;
        }
        if (task.IsFaulted) {
            Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
            return;
        }     

        // Firebase user has been created.
        Firebase.Auth.FirebaseUser newUser = task.Result;
        Debug.LogFormat("Firebase user created successfully: {0} ({1})",
        newUser.DisplayName, newUser.UserId);
        });
    }

    // Handle initialization of the necessary firebase modules:

    void OnDestroy() {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }

    public void GetSessionProfile(){
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null) {
            string name = user.DisplayName;
            string email = user.Email;
            System.Uri photo_url = user.PhotoUrl;
            // The user's Id, unique to the Firebase project.
            // Do NOT use this value to authenticate with your backend server, if you
            // have one; use User.TokenAsync() instead.
            string uid = user.UserId;
            Debug.Log("Name: "+name+"Email: "+email);
        }
    }

}
