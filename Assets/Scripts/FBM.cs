﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Facebook.Unity;
using PlayFab.ClientModels;

public class FBM : MonoBehaviour
{
    public static FBM Instance;
    //public string userName;
    //public Sprite userProfilePic;

    [SerializeField] private Text _nameText;
    [SerializeField] private Image _profileImage;
    [SerializeField] private Sprite _noProfileSprite;
    [SerializeField] private GameObject _postLoginActions;
    [SerializeField] private Text _scoreText;

    void Awake(){
        DontDestroyOnLoad (this);
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(this.gameObject);
        }        Instance = this;
    }
    private void Start()
    {
        if (FacebookAndPlayFabManager.Instance.IsLoggedOnFacebook)
        {
            _nameText.text = FacebookAndPlayFabManager.Instance.FacebookUserName;
            _profileImage.sprite = FacebookAndPlayFabManager.Instance.FacebookUserPictureSprite;
            _postLoginActions.SetActive(true);
        }
    }

    public void LoginWithFacebook()
    {
        if (string.IsNullOrEmpty(FacebookAndPlayFabManager.Instance.PlayFabTitleId))
        {
            Debug.LogError("PlayFabTitleId is null.");
            return;
        }

        if (FacebookAndPlayFabManager.Instance.IsLoggedOnFacebook)
            return;

        FacebookAndPlayFabManager.Instance.LogOnFacebook(successCallback: res =>
        {
            StartCoroutine(GetUserNameRoutine());
            StartCoroutine(GetUserPictureRoutine());
            StartCoroutine(WaitForPlayFabLogin());
        });
    }

    // Shows the player's Facebook name as soon as it's available.
    private IEnumerator GetUserNameRoutine()
    {
        yield return new WaitUntil(() => !string.IsNullOrEmpty(FacebookAndPlayFabManager.Instance.FacebookUserName));

        //_nameText.text = FacebookAndPlayFabManager.Instance.FacebookUserName;
    }

    // Shows the player's Facebook picture as soon as it's available.
    private IEnumerator GetUserPictureRoutine()
    {
        yield return new WaitUntil(() => FacebookAndPlayFabManager.Instance.FacebookUserPictureSprite != null);

        //_profileImage.sprite = FacebookAndPlayFabManager.Instance.FacebookUserPictureSprite;
    }

    public void UpdateFacebookUserUI(){
        Debug.LogWarning("2");
        foreach (var item in UIElementsMenu.Instance.userNameText)
        {
            item.GetComponent<Text>().text = FacebookAndPlayFabManager.Instance.FacebookUserName;
        }
        foreach (var item in UIElementsMenu.Instance.userProfilePic)
        {
            item.GetComponent<Image>().sprite = FacebookAndPlayFabManager.Instance.FacebookUserPictureSprite;
        }
        GetLeaderboard();
    }
    // Enable a set of buttons as soon as the player gets logged on PlayFab.
    private IEnumerator WaitForPlayFabLogin()
    {
        yield return new WaitUntil(() => FacebookAndPlayFabManager.Instance.IsLoggedOnPlayFab);

        //_postLoginActions.SetActive(true);
        SceneManager.LoadScene(1);
    }

    public void Logout()
    {
        FacebookAndPlayFabManager.Instance.LogOutFacebook();
        _profileImage.sprite = _noProfileSprite;
        _nameText.text = "My Facebook Name";
        _postLoginActions.SetActive(false);
    }

    public void PostScoreOnPlayFab()
    {
        if (string.IsNullOrEmpty(_scoreText.text))
            return;

        var score = System.Convert.ToInt32(_scoreText.text);

        FacebookAndPlayFabManager.Instance.UpdateStat(Constants.LeaderboardName, score);
    }

    void GetLeaderboard()
    {
        FacebookAndPlayFabManager.Instance.GetLeaderboard("PKP", false, 7, GetLeaderboardCallback, 0);
    }

     public void GetLeaderboardCallback(GetLeaderboardResult result)
    {
        for (int i = 0; i < result.Leaderboard.Count; i++)
        {
            LeaderboardEntry entry = UIElementsMenu.Instance.leaderBoardUIs[i];
            PlayerLeaderboardEntry playerEntry = result.Leaderboard[i];

            entry.SetUserScore(result.Leaderboard[i].StatValue.ToString()+" PKP");
            entry.SetUserPosition(result.Leaderboard[i].Position+1);
            if (playerEntry.DisplayName == FacebookAndPlayFabManager.Instance.FacebookUserId)
            {
                entry.SetUserName(FacebookAndPlayFabManager.Instance.FacebookUserName);
                entry.SetUserPictureSprite(FacebookAndPlayFabManager.Instance.FacebookUserPictureSprite);
            }
            else
            {
                FacebookAndPlayFabManager.Instance.GetFacebookUserName(playerEntry.DisplayName, res =>
                {
                    entry.SetUserName(res.ResultDictionary["name"].ToString());
                });

                FacebookAndPlayFabManager.Instance.GetFacebookUserPicture(playerEntry.DisplayName, 100, 100, res =>
                {
                    entry.SetUserPictureSprite(ImageUtils.CreateSprite(res.Texture, new Rect(0, 0, 100, 100), Vector2.zero));
                });
                //FacebookAndPlayFabManager.Instance.GetFacebookUserPictureFromUrl(playerEntry.DisplayName, width, height, res =>
                //{
                //    StartCoroutine(FacebookAndPlayFabManager.Instance.GetTextureFromGraphResult(res, tex =>
                //    {
                //        entry.SetUserPictureSprite(Sprite.Create(tex, new Rect(0, 0, width, height), Vector2.zero));
                //    }));
                //});
            }
        }
    }
}
