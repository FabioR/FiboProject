﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

public class Loader : MonoBehaviour {
    public int numberOfCollums = 8;
    private int progress = 0;
    List<string> languages = new List<string>();
    private string[] questionsInf;
    public List<string> questionsInfo = new List<string>();
    public List<string[]> Questions = new List<string[]>();
    void Start() {
        Load();
    }

    public void Load() {
        StartCoroutine(CSVDownloader.DownloadData( AfterDownload ) );
    }

    public void AfterDownload( string data ) {
        if ( null == data ) {
            Debug.LogError( "Was not able to download data or retrieve stale data." );
            // TODO: Display a notification that this is likely due to poor internet connectivity
            //       Maybe ask them about if they want to report a bug over this, though if there's no internet I guess they can't
        }
        else {
           // StartCoroutine( ProcessData( data, AfterProcessData ) );
        }
    }

    private void AfterProcessData( string errorMessage ) {
        if ( null != errorMessage ) {
            Debug.LogError( "Was not able to process data: " + errorMessage );
            // TODO: 
        }
        else {

        }
    }

    public IEnumerator ProcessData( string data, System.Action<string> onCompleted ) {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        Debug.Log("Loading data from "+((data == null)?"local ":"remote ")+"source");
        string da = data.Replace(System.Environment.NewLine, ",");
        Debug.Log(da);

        questionsInf = da.Split(',');

        questionsInfo = new List<string>(questionsInf); 

        for (int i = 0; i < questionsInfo.Count; i++)
        {
            questionsInfo[i] = questionsInfo[i].Replace("_",",");
        }

        int counter =1;
        int counterIndex =0;
        for (int i = 0; i < questionsInfo.Count/numberOfCollums; i++)
        {
            Question2 quest = (Question2)ScriptableObject.CreateInstance(typeof(Question2));

            quest._info = questionsInfo[2+counterIndex];
            quest._answers[0]._info = questionsInfo[3+counterIndex];
            quest._answers[1]._info = questionsInfo[4+counterIndex];
            quest._answers[2]._info = questionsInfo[5+counterIndex];
            quest._answers[3]._info = questionsInfo[6+counterIndex];

            switch (questionsInfo[7+counterIndex])
            {
                case "A":
                    quest._answers[0]._isCorrect = true;
                    break;
                case "B":
                    quest._answers[1]._isCorrect = true;
                    break;
                case "C":
                    quest._answers[2]._isCorrect = true;
                    break;
                case "D":
                    quest._answers[3]._isCorrect = true;
                    break;
                default:
                    quest._answers[0]._isCorrect = true;
                    break;
            }
            ScriptableObject question2 = ScriptableObject.CreateInstance("Question2");
            AssetDatabase.CreateAsset(quest, "Assets/Resources/"+questionsInfo[0+counterIndex]+"_"+questionsInfo[1+counterIndex]+"/Pergunta_"+counter+".asset");
            //AssetDatabase.CreateAsset(quest, "Assets/Resources/Física_Easy/Pergunta_"+counter+".asset");
            
            counter++;
            counterIndex+=numberOfCollums;
        } 
        questionsInfo.Clear();
    }
}