﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable()]
public struct Answer2
{
    [SerializeField] public string _info;
    public string Info { get { return _info; } }

    [SerializeField] public bool _isCorrect;
    public bool IsCorrect { get { return _isCorrect; } }
}
[CreateAssetMenu(fileName = "New Question", menuName = "Quiz/new Question")]
public class Question2 : ScriptableObject {

    public enum                 AnswerType2                  { Multi, Single }

    [SerializeField] public    String      _info           = String.Empty;
    public                      String      Info            { get { return _info; } }

    [SerializeField]public      Answer2[]    _answers        = new Answer2[4];
    public                      Answer2[]    Answers         { get { return _answers; } }

    //Parameters

    [SerializeField] private    bool        _useTimer       = true;
    public                      bool        UseTimer        { get { return _useTimer; } }

    [SerializeField] private    int         _timer          = 10;
    public                      int         Timer           { get { return _timer; } }

    [SerializeField] private    AnswerType2  _answerType     = AnswerType2.Single;
    public                      AnswerType2  GetAnswerType   { get { return _answerType; } }

    [SerializeField] private    int         _addScore       = 10;
    public                      int         AddScore        { get { return _addScore; } }

    [SerializeField] private    Sprite      _questionImage;
    public                      Sprite      QuestionImage{ get { return _questionImage; } }

    /// <summary>
    /// Function that is called to collect and return correct answers indexes.
    /// </summary>
    public List<int> GetCorrectAnswers ()
    {
        List<int> CorrectAnswers = new List<int>();
        for (int i = 0; i < Answers.Length; i++)
        {
            if (Answers[i].IsCorrect)
            {
                CorrectAnswers.Add(i);
            }
        }
        return CorrectAnswers;
    }
}