﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class AplicationManager : MonoBehaviour
{
    public bool facebookLogin = false;
    public static AplicationManager instance;
    public Text debugText;
    private bool exitEscape;
    public List<Text> userNameLabels = new List<Text>();
    public List<Image> profilePics = new List<Image>();
    public Text rankText;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        //PlayerPrefs.DeleteAll();
        if(!PlayerPrefs.HasKey("Linguagens"))
            CreateDKPScores();

    }

    void Start(){
        UpdatePlayerRank();
        try{
            //FacebookManager.instance.InitializeUI(); 
        }
        catch(System.Exception){}
    }

    void UpdatePlayerRank(){
        float pkp = PlayerPrefs.GetFloat("PKP");

        if(pkp < 200){
            PlayerPrefs.SetInt("PlayerTier", 1);
        }
        else if(pkp < 400){
            PlayerPrefs.SetInt("PlayerTier", 2);
        }
        else if(pkp < 600){
            PlayerPrefs.SetInt("PlayerTier", 3);
        }
        else if(pkp < 800){
            PlayerPrefs.SetInt("PlayerTier", 4);
        }
        else if(pkp < 1000){
            PlayerPrefs.SetInt("PlayerTier", 5);
        }
        else{
            PlayerPrefs.SetInt("PlayerTier", 6);
        }

//        rankText.text = pkp.ToString();
    }
    void CreateDKPScores(){
        //PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("Linguagens", 100);
        PlayerPrefs.SetInt("Biologia", 100);
        PlayerPrefs.SetInt("Geografia", 100);
        PlayerPrefs.SetInt("Artes", 100);
        PlayerPrefs.SetInt("Matemática", 100);
        PlayerPrefs.SetInt("Filosofia", 100);
        PlayerPrefs.SetInt("Física", 100);
        PlayerPrefs.SetInt("História", 100);
        PlayerPrefs.SetInt("Química", 100);
    }
    void CopyQuestionToMobile (string fileName) {
        string dataPath = Application.persistentDataPath + "/" + fileName;
        string assetPath = Application.dataPath+"Assets/Resources/" + fileName;  
        //debugText.text += "  "+assetPath;

         if(!File.Exists(dataPath)) {
             // File doesn't exist, move it from assets folder to data directory
             File.Copy(assetPath, dataPath);
         }else{
            debugText.text += dataPath;
         }

    }
     
     void Update(){
        //  if(Input.GetKeyDown(KeyCode.Escape) && exitEscape){
        //      Application.Quit();
        //      Debug.Log("Saiu");
        //  }
        //  else if(Input.GetKeyDown(KeyCode.Escape)){
        //      exitEscape = true;
        //     Debug.Log("QUER SAIR");
        //  }
     }

    public void FacebookUIProfileInformations(string userName, Sprite profilePic){
        if(facebookLogin){
            // GameObject[] unl = GameObject.FindGameObjectsWithTag("FacebookUserName");
            // GameObject[] upl = GameObject.FindGameObjectsWithTag("FacebookProfilePic");
            // Debug.Log(unl.Length);
            // Debug.Log(upl.Length);
            // foreach (var nam in unl)
            // {
            //     userNameLabels.Add(nam.GetComponent<Text>());
            // }
            // foreach (var nam in upl)
            // {
            //     profilePics.Add(nam.GetComponent<Image>());
            // }  
            Debug.Log(userName);
            foreach (var item in userNameLabels)
            {
                item.text = userName;
            }
            foreach (var item in profilePics)
            {
                item.sprite = profilePic;
            }     
        }
    }
}