﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonForAchievementsMenu : MonoBehaviour
{
    public int themeId;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(delegate() { AchivementSys.Instance.AchievementUIRefreshTheme(themeId); });

    }

}
