﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using System.Collections.Generic;

public class GameUtility {
    public static List<int> sortedThemes = new List<int> {0,1,2,3,4,5};

    public const float      ResolutionDelayTime     = 1.5f;
    public const float      ThemeDelayTime     = 1.5f;
    public const string     SavePKPKey             = "PKP_Value";
    
    public const string     FileName                = "Q";
    public static string    FileDir                 
    {
        get
        {
            return Application.persistentDataPath + "/";
        }
    }

    public static string ThemeNameText(int themeId)
    {
        switch (themeId)
        {
            case 0:
                return "Linguagens";
            case 1:
                return "Biologia";
            case 2:
                return "Geografia";
            case 3:
                return "Artes";
            case 4:
                return "Matemática";
            case 5:
                return "Filosofia";
            case 6:
                return "Física";
            case 7:
                return "História";
            case 8:
                return "Química";
            default:
                return "Meme";
        }
    }

    public static int ThemeNameText(string themeId)
    {
        switch (themeId)
        {
            case "Linguagens":
                return 0;
            case "Biologia":
                return 1;
            case "Geografia":
                return 2;
            case "Artes":
                return 3;
            case "Matemática":
                return 4;
            case "Filosofia":
                return 5;
            case "Física":
                return 6;
            case "História":
                return 7;
            case "Química":
                return 8;
            default:
                return 10;
        }
    }
}


[System.Serializable()]
public class Data
{
    public Question[] Questions = new Question[0];
    public Question2[] Questions2 = new Question2[0];

    public Data () { }

    public static void Write(Data data, string path)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Data));
        using (Stream stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, data);
        }
    }
    public static Data Fetch(string filePath)
    {
        return Fetch(out bool result, filePath);
    }
    public static Data Fetch(out bool result, string filePath)
    {
        if (!File.Exists(filePath)) { result = false; return new Data(); }

        XmlSerializer deserializer = new XmlSerializer(typeof(Data));
        using (Stream stream = new FileStream(filePath, FileMode.Open))
        {
            var data = (Data)deserializer.Deserialize(stream);

            result = true;
            return data;
        }
    }
}