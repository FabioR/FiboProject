﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIElementsMenu : MonoBehaviour
{
    public static UIElementsMenu Instance;
    public GameObject[] userNameText;
    public GameObject[] userProfilePic;
    public LeaderboardEntry[] leaderBoardUIs;
    public List<AchivementsUIElements> achievementsListsUI;

    public List<Color32> incompleteBarColors = new List<Color32>();
    public List<Color32> completeBarColors = new List<Color32>();
    public Text totalAchievemetsText;
    public Text[] fiboLevelPKPText;
    public Text[] correctsRateTexts;
    public Text correctsAnswersText;
    public Text totalAnswersText;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        //userNameText = GameObject.FindGameObjectsWithTag("FacebookUserName");
        //userProfilePic = GameObject.FindGameObjectsWithTag("FacebookProfilePic");
        fiboLevelPKPText[0].text = PlayerPrefs.GetFloat("PKP").ToString();
        //fiboLevelPKPText[1].text = PlayerPrefs.GetFloat("PKP").ToString();
        correctsRateTexts[0].text = PlayerPrefs.GetFloat("HighScoreCorrectsRate").ToString("00");
        correctsRateTexts[1].text = PlayerPrefs.GetFloat("HighScoreCorrectsRate").ToString("00");
        correctsAnswersText.text =  PlayerPrefs.GetInt("Fibo").ToString();
        totalAnswersText.text = PlayerPrefs.GetInt("QuestionsAnswered").ToString();

        AchivementSys.Instance.InitializeAchievementsInMainMenu();
        StartCoroutine(CallBackFBUI());
    }

    // Update is called once per frame
    IEnumerator CallBackFBUI()
    {
        yield return new WaitUntil(() => !string.IsNullOrEmpty(FacebookAndPlayFabManager.Instance.FacebookUserName));
        yield return new WaitUntil(() => FacebookAndPlayFabManager.Instance.FacebookUserPictureSprite != null);
        FBM.Instance.UpdateFacebookUserUI();
    }
}

[System.Serializable()]
public struct AchivementsUIElements{
    [SerializeField]
    public Text achievementNameText;
    [SerializeField]
    public Text achievementDescriptionText;
    [SerializeField]
    public Text achievementProgressPercentageText;
    [SerializeField]
    public Slider achievemenProgressBar;

}
