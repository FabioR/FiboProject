﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable()]
public class AchievementInfo{
    [SerializeField]
    public string achievementName;
    //[SerializeField]
    //public string achievementDescription;
    [SerializeField]
    public int achievementProgressPercentageMax;
    [SerializeField]
    public int achievementProgressPercentageCounter;
    public bool isComplete;
}
public class AchivementSys : MonoBehaviour
{
    private const string level1Nomenclature = "Iniciante";
    private const string level2Nomenclature = "Esforçado";
    private const string level3Nomenclature = "Estudioso";
    private const string level4Nomenclature = "Veterano";
    private const string level5Nomenclature = "Resiliente";
    private const string level6Nomenclature = "Gênio";

    public static AchivementSys Instance;
    //public Dictionary<int,int> achievementsIndex = new Dictionary<int, int>();
    public List<AchievementInfo> achievementList;
    public List<int> scoreForEachTheme = new List<int>();
    public int achievementQuantity;

    // private int initialFiboCount;
    // private int correctAnswers;

    private int fiboAnswers;
     #region ThemesVars
    private int linguisticaCorrectAnswers, biologiaCorrectAnswers,geografiaCorrectAnswers,artesCorrectAnswers,
    matematicaCorrectAnswers,filosofiaCorrectAnswers,fisicaCorrectAnswers,historyCorrectAnswers,quimicaCorrectAnswers;
    #endregion

    void Awake()
    {
        DontDestroyOnLoad (this);
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(this.gameObject);
        }

        //InitializeAchievementsIndexDictionary();
    }

    public void InitializeAchievementsInMainMenu(){
        if(PlayerPrefs.HasKey("LingHits")){linguisticaCorrectAnswers = PlayerPrefs.GetInt("LingHits");}
        else{linguisticaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("BioHits")){biologiaCorrectAnswers = PlayerPrefs.GetInt("BioHits");}
        else{biologiaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("GeoHits")){geografiaCorrectAnswers = PlayerPrefs.GetInt("GeoHits");}
        else{geografiaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("ArtHits")){artesCorrectAnswers = PlayerPrefs.GetInt("ArtHits");}
        else{artesCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("FilHits")){filosofiaCorrectAnswers = PlayerPrefs.GetInt("FilHits");}
        else{filosofiaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("FisHits")){fisicaCorrectAnswers = PlayerPrefs.GetInt("FisHits");}
        else{fisicaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("MatHits")){matematicaCorrectAnswers = PlayerPrefs.GetInt("MatHits");}
        else{matematicaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("HistoryHits")){historyCorrectAnswers = PlayerPrefs.GetInt("HistoryHits");}
        else{historyCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("QuiHits")){quimicaCorrectAnswers = PlayerPrefs.GetInt("QuiHits");}
        else{quimicaCorrectAnswers = 0;}
        if(PlayerPrefs.HasKey("Fibo")){fiboAnswers = PlayerPrefs.GetInt("Fibo");}
        else{fiboAnswers = 0;}
        scoreForEachTheme.Clear();
        scoreForEachTheme.Add(linguisticaCorrectAnswers);scoreForEachTheme.Add(biologiaCorrectAnswers);
        scoreForEachTheme.Add(geografiaCorrectAnswers);scoreForEachTheme.Add(artesCorrectAnswers);
        scoreForEachTheme.Add(filosofiaCorrectAnswers);scoreForEachTheme.Add(fisicaCorrectAnswers);
        scoreForEachTheme.Add(matematicaCorrectAnswers);scoreForEachTheme.Add(historyCorrectAnswers);
        scoreForEachTheme.Add(quimicaCorrectAnswers);scoreForEachTheme.Add(fiboAnswers);

        achievementQuantity = 0;
        AchievementUIRefreshTheme(9);
        VerifyTotalOfAchievementsRewarded();
    }

    public void AchievementUIRefreshTheme(int theme){
        string themeName;
        int value = scoreForEachTheme[theme];

        if(theme == 9)
            themeName = "Fibo";
        else
            themeName = GameUtility.ThemeNameText(theme);

        UIElementsMenu.Instance.achievementsListsUI[0].achievementNameText.text = level1Nomenclature+" em "+themeName;
        UIElementsMenu.Instance.achievementsListsUI[1].achievementNameText.text = level2Nomenclature+" em "+themeName;
        UIElementsMenu.Instance.achievementsListsUI[2].achievementNameText.text = level3Nomenclature+" em "+themeName;
        UIElementsMenu.Instance.achievementsListsUI[3].achievementNameText.text = level4Nomenclature+" em "+themeName;
        UIElementsMenu.Instance.achievementsListsUI[4].achievementNameText.text = level5Nomenclature+" em "+themeName;
        UIElementsMenu.Instance.achievementsListsUI[5].achievementNameText.text = level6Nomenclature+" em "+themeName;

        for (int i = 0; i < 6; i++)
        {
            int maxValue = (int)UIElementsMenu.Instance.achievementsListsUI[i].achievemenProgressBar.maxValue;

            UIElementsMenu.Instance.achievementsListsUI[i].achievementDescriptionText.text = 
            "Responda "+maxValue+" perguntas corretamente";

            if(value >= maxValue){
                UIElementsMenu.Instance.achievementsListsUI[i].achievementProgressPercentageText.text =  maxValue+"/"+maxValue;
                UIElementsMenu.Instance.achievementsListsUI[i].achievemenProgressBar.value = maxValue;
                UIElementsMenu.Instance.achievementsListsUI[i].achievemenProgressBar.transform.GetChild(1).
                GetComponentInChildren<Image>().color = UIElementsMenu.Instance.completeBarColors[theme];
            }
            else{
                UIElementsMenu.Instance.achievementsListsUI[i].achievementProgressPercentageText.text = value+"/"+maxValue;
                UIElementsMenu.Instance.achievementsListsUI[i].achievemenProgressBar.value = value;
                UIElementsMenu.Instance.achievementsListsUI[i].achievemenProgressBar.transform.GetChild(1).
                GetComponentInChildren<Image>().color = UIElementsMenu.Instance.incompleteBarColors[theme];
            }
            
        }
    }

    public void VerifyTotalOfAchievementsRewarded(){
        for (int i = 0; i < scoreForEachTheme.Count; i++)
        {
            if(scoreForEachTheme[i] >=10)achievementQuantity++;
            if(scoreForEachTheme[i] >=25)achievementQuantity++;
            if(scoreForEachTheme[i] >=50)achievementQuantity++;
            if(scoreForEachTheme[i] >=100)achievementQuantity++;
            if(scoreForEachTheme[i] >=250)achievementQuantity++;
            if(scoreForEachTheme[i] >=500)achievementQuantity++;
        }
        UIElementsMenu.Instance.totalAchievemetsText.text = achievementQuantity.ToString();
    }
}
