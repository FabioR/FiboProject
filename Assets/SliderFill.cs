﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderFill : MonoBehaviour
{
    // Awake is called before the first frame update
    public Animator thisAnimator;
    public Slider thisSlider;
 
    void Awake()
    {
        thisAnimator = GetComponent<Animator>();
        thisSlider = GetComponent<Slider>();
    }

    void Update()
    {
        //thisAnimator.SetFloat("FillValue", thisSlider.value);
    }
    public void UpdateAnimatorParameter(float sliderFill){
        //Debug.Log(sliderFill);
        thisAnimator.SetFloat("FillValue", sliderFill);
    }
}
